import random

from Domain.ReprezentareDate import Inscriere
from Erori.Exceptii import ServiceExceptie
from Sortari import Shell_sort, Bubble_sort
from Validare.Validari import ValidatorInscriere, ValidatorPersoana, ValidatorEveniment


class ServiceInscriere:
    """
    Clasa ServiceInscriere contine metode inscriere o persoana la un eveniment, de sterge persoana de la toate evenimentele la care participa
    """

    def __init__(self, repository_persoana, repository_eveniment, repository_inscriere, validator_inscriere):
        self.__repository_persoana = repository_persoana
        self.__repository_eveniment = repository_eveniment
        self.__repository_inscriere = repository_inscriere
        self.__validator_inscriere = validator_inscriere

    def adaugare_inscriere(self, person_id, eveniment_id):
        """
        Metoda de adaugare a unei inscrieri
        """
        persoane = self.__repository_persoana.get_all()
        inscriere_id = "I"
        inscriere_id = inscriere_id + str(random.randint(1000, 9999))
        persoana = self.__repository_persoana.get_person_by_id_recursive(persoane, person_id)
        eveniment = self.__repository_eveniment.get_eveniment_by_id(eveniment_id)
        inscriere = Inscriere(inscriere_id, persoana, eveniment)
        validator = ValidatorInscriere()
        validator.valideaza(inscriere)
        self.__repository_inscriere.adaugare(inscriere)

    def get_inscrieri_nou(self):
        persoane = self.__repository_persoana.get_all()
        lista_inscrieri = self.__repository_inscriere.get_all()
        lista_rezultat = []
        for i in range(0, len(lista_inscrieri)):
            persoana = self.__repository_persoana.get_person_by_id_recursive(persoane, lista_inscrieri[i][1])
            eveniment = self.__repository_eveniment.get_eveniment_by_id(lista_inscrieri[i][2])
            inscriere = Inscriere(lista_inscrieri[i][0], persoana, eveniment)
            lista_rezultat.append(inscriere)
        return lista_rezultat

    def remove_inscriere_si_persoana_by_id(self, person_id):
        """
        Metoda de stergere a unei persoane si inscrierile acesteia
        """
        persoane = self.__repository_persoana.get_all()
        lista_inscrieri = self.__repository_inscriere.get_all()
        for i in range(0, len(lista_inscrieri)):
            if self.__repository_persoana.get_person_by_id_recursive(persoane, lista_inscrieri[i][1]).get_person_id() == person_id:
                self.__repository_inscriere.remove_inscriere_by_id(lista_inscrieri[i][0])
        self.__repository_persoana.remove(person_id)

    def remove_inscriere_si_evenimente_by_id(self, eveniment_id):
        """
        Metoda de stergere a unei persoane si inscrierile acesteia
        """
        lista_inscrieri = self.__repository_inscriere.get_all()
        for i in range(0, len(lista_inscrieri)):
            if self.__repository_eveniment.get_eveniment_by_id(lista_inscrieri[i][2]).get_eveniment_id() == eveniment_id:
                self.__repository_inscriere.remove_inscriere_by_id(lista_inscrieri[i][0])
        self.__repository_eveniment.remove(eveniment_id)

    def modificare_persoana(self, person_id_vechi, persoana):
        lista_inscrieri = self.__repository_inscriere.get_all()
        persoane = self.__repository_persoana.get_all()
        for i in range(0, len(lista_inscrieri)):
            if self.__repository_persoana.get_person_by_id_recursive(persoane, lista_inscrieri[i][1]).get_person_id() == person_id_vechi:
                self.__repository_inscriere.update_inscriere_persoana_by_id(lista_inscrieri[i][0], persoana.get_person_id())
        validator = ValidatorPersoana()
        validator.valideaza(persoana)
        self.__repository_persoana.update(person_id_vechi, persoana)

    def modificare_eveniment(self, eveniment_id_vechi, eveniment):
        lista_inscrieri = self.__repository_inscriere.get_all()
        for i in range(0, len(lista_inscrieri)):
            if self.__repository_eveniment.get_eveniment_by_id(lista_inscrieri[i][2]).get_eveniment_id() == eveniment_id_vechi:
                self.__repository_inscriere.update_inscriere_eveniment_by_id(lista_inscrieri[i][0], eveniment.get_eveniment_id())
        validator = ValidatorEveniment()
        validator.valideaza(eveniment)
        self.__repository_eveniment.update(eveniment_id_vechi, eveniment)

    def afisare_evenimente_persoana_sortat_data(self, person_id):
        """
        Metoda de afisare a evenimentelor la care va participa o persoana ordonate dupa data
        """
        lista = self.__repository_inscriere.lista_evenimente_persoana(person_id)
        lista_evenimente = []
        for i in range(0, len(lista)):
            eveniment = self.__repository_eveniment.get_eveniment_by_id(lista[i])
            lista_evenimente.append(eveniment)
        return Bubble_sort(lista_evenimente, key=lambda data: data.get_data())

    def afisare_evenimente_persoana_sortat_descriere(self, person_id):
        lista = self.__repository_inscriere.lista_evenimente_persoana(person_id)
        lista_evenimente = []
        for i in range(0, len(lista)):
            eveniment = self.__repository_eveniment.get_eveniment_by_id(lista[i])
            lista_evenimente.append(eveniment)
        return Shell_sort(lista_evenimente, key=lambda parametru: parametru.get_descriere())

    def numarul_de_evenimente_persoana(self):
        """
        Metoda de afisarea a persoanelor participante la cele mai multe evenimente
        """
        dictionar = {}
        rezultat = []
        rezultat_persoane = []
        persoane = self.__repository_persoana.get_all()
        inscrieri = self.__repository_inscriere.get_all()
        for i in range(0, len(persoane)):
            dictionar[persoane[i].get_person_id()] = []
        for i in range(0, len(inscrieri)):
            dictionar[inscrieri[i][1]].append([self.__repository_eveniment.get_eveniment_by_id(inscrieri[i][2])])
        for cheie in dictionar.keys():
            rezultat.append([cheie, len(dictionar[cheie])])
        maxim = max(rezultat, key=lambda x: x[1])
        for i in range(0, len(rezultat)):
            if rezultat[i][1] == maxim[1] and rezultat[i][1] != 0:
                rezultat_persoane.append([str(self.__repository_persoana.get_person_by_id(rezultat[i][0])), rezultat[i][1]])
        return rezultat_persoane

    def numarul_de_persoane_eveniment(self):
        """
        Metoda de afisare a primelor 20% evenimente cu cele mai multe persoane
        """
        dictionar = {}
        rezultat = []
        rezultat_evenimente = []
        evenimente = self.__repository_eveniment.get_all()
        inscrieri = self.__repository_inscriere.get_all()
        persoane = self.__repository_persoana.get_all()
        for i in range(0, len(evenimente)):
            dictionar[evenimente[i].get_eveniment_id()] = []
        for i in range(0, len(inscrieri)):
            dictionar[inscrieri[i][2]].append(self.__repository_persoana.get_person_by_id_recursive(persoane, inscrieri[i][1]))
        for cheie in dictionar.keys():
            if len(dictionar[cheie]) != 0:
                rezultat.append([cheie, len(dictionar[cheie])])
        if len(rezultat) < 5:
            raise ServiceExceptie("Pentru a efectua acest raport trebuie sa avem cel putin 5 evenimente care se vor desfasura!")
        rezultat.sort(key=lambda parametru: parametru[1], reverse=True)
        for i in range(0, int(len(rezultat)/5)):
            if rezultat[i][1] != 0:
                rezultat_evenimente.append([self.__repository_eveniment.get_eveniment_by_id(rezultat[i][0]), rezultat[i][1]])
        return rezultat_evenimente

    def persoane_participante_evenimente_zi(self, data):
        dictionar = {}
        rezultat = []
        rezultat_persoane = []
        persoane = self.__repository_persoana.get_all()
        inscrieri = self.__repository_inscriere.get_all()
        for i in range(0, len(persoane)):
            dictionar[persoane[i].get_person_id()] = []
        for i in range(0, len(inscrieri)):
            if str(self.__repository_eveniment.get_eveniment_by_id(inscrieri[i][2]).get_data()) == data:
                dictionar[inscrieri[i][1]].append([self.__repository_eveniment.get_eveniment_by_id(inscrieri[i][2])])
        for cheie in dictionar.keys():
            rezultat.append([cheie, len(dictionar[cheie])])
        rezultat.sort(key=lambda parametru: parametru[1], reverse=True)
        for i in range(0, len(rezultat)):
            if rezultat[i][1] != 0:
                rezultat_persoane.append([self.__repository_persoana.get_person_by_id(rezultat[i][0]), rezultat[i][1]])
        return rezultat_persoane
