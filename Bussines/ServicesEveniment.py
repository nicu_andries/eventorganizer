import string
from Domain.ReprezentareDate import Eveniment
from Validare.Validari import ValidatorEveniment
import random


class ServiceEveniment:
    """
    Clasa ServiceEveniment contine functiile de adaugare, modificare, stergere, cautare pentru evenimente
    """
    def __init__(self, repository_eveniment, validator_eveniment):
        self.__repository_eveniment = repository_eveniment
        self.__validator_eveniment = validator_eveniment

    def adaugare_eveniment(self, eveniment_id, data, timp, descriere):
        validator = ValidatorEveniment()
        eveniment = Eveniment(eveniment_id, data, timp, descriere)
        validator.valideaza(eveniment)
        self.__repository_eveniment.store(eveniment)

    def adaugare_eveniment_random(self, numar_evenimente):
        if numar_evenimente < 1:
            raise("Numarul de evenimente trebuie sa fie mai mare ca 1!")
        for i in range(0, numar_evenimente):
            validator = ValidatorEveniment()
            eveniment_id = random.choice(string.ascii_uppercase)
            eveniment_id = eveniment_id + str(random.randint(100, 999))
            data = str(random.randint(2020, 2050))
            data = data + "." + str(random.randint(10, 12))
            data = data + "." + str(random.randint(10, 30))
            timp = str(random.randint(10, 23))
            timp = timp + "." + str(random.randint(10, 59))
            descriere = "Descriere"
            eveniment = Eveniment(eveniment_id, data, timp, descriere+random.choice(string.ascii_lowercase))
            validator.valideaza(eveniment)
            self.__repository_eveniment.store(eveniment)

    def modificare_eveniment(self, eveniment_id, data, timp, descriere):
        validator = ValidatorEveniment
        eveniment = Eveniment(eveniment_id, data, timp, descriere)
        validator.valideaza(eveniment)
        self.__repository_eveniment.update(eveniment)

    def cautare_dupa_data(self, data):
        return self.__repository_eveniment.search_by_data(data)

    def cautare_dupa_descriere(self, descriere):
        return self.__repository_eveniment.search_by_description(descriere)

    def get_nr_evenimente(self):
        return len(self.__repository_eveniment)

    def get_evenimente(self):
        return self.__repository_eveniment.get_all()
