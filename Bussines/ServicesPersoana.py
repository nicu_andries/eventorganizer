from Domain.ReprezentareDate import Persoana
from Erori.Exceptii import ServiceExceptie
from Sortari import Bubble_sort_doua_chei, Bubble_sort, Shell_sort
from Validare.Validari import ValidatorPersoana
import random
import names


class ServicePersoana:
    """
    Clasa ServicePersoana contine functiile de adaugare, modificare, stergere, cautare pentru persoane
    """

    def __init__(self, repository_persoana, validator_persoana):
        self.__repository_persoana = repository_persoana
        self.__validator_persoana = validator_persoana

    def adaugare_persoana(self, person_id, nume, adresa):
        validator = ValidatorPersoana()
        persoana = Persoana(person_id, nume, adresa)
        validator.valideaza(persoana)
        self.__repository_persoana.store(persoana)

    def adaugare_persoana_random(self, numar_persoane):
        if numar_persoane < 1:
            raise ServiceExceptie("Numarul de persoane trebuie sa fie mai mare ca 0!")
        for i in range(0, numar_persoane):
            validator = ValidatorPersoana()
            person_id = str(random.randint(1000, 9999))
            nume = names.get_full_name()
            adresa = "str. Adresa nr."
            persoana = Persoana(person_id, nume, adresa + str(i))
            validator.valideaza(persoana)
            self.__repository_persoana.store(persoana)

    def modificare_persoana(self, person_id_vechi, persoana):
        validator = ValidatorPersoana()
        validator.valideaza(persoana)
        self.__repository_persoana.update(person_id_vechi, persoana)

    def cautare_dupa_adresa(self, adresa):
        persoane = self.__repository_persoana.get_all()
        return self.__repository_persoana.search_by_adresa(persoane, adresa)

    def sortare_persoane_bubble_doua_chei(self):
        persoane = self.__repository_persoana.get_all()
        lista_sortata = Bubble_sort_doua_chei(persoane, key=lambda cheie: cheie.get_adresa(), key1=lambda cheie1: cheie1.get_nume())
        return lista_sortata

    def sortare_persoane_bubble(self):
        persoane = self.__repository_persoana.get_all()
        lista_sortata = Bubble_sort(persoane, key=lambda cheie: cheie.get_adresa())
        return lista_sortata

    def sortare_persoane_shell(self):
        persoane = self.__repository_persoana.get_all()
        lista_sortata = Shell_sort(persoane, key=lambda cheie: cheie.get_nume())
        return lista_sortata

    def cautare_dupa_litera(self, litera):
        return self.__repository_persoana.search_by_first_letter(litera)

    def get_nr_persoane(self):
        return len(self.__repository_persoana)

    def get_persoane(self):
        return self.__repository_persoana.get_all()
