from Bussines.ServiceInscriere import ServiceInscriere
from Bussines.ServicesEveniment import ServiceEveniment
from Bussines.ServicesPersoana import ServicePersoana
from Infastructure.Repositories import RepositoryPersoana, RepositoryEveniment, RepositoryInscriere
from Infastructure.RepositoryFile import EvenimentFileRepository, InscriereFileRepository, PersoanaFileRepository
from Presentation.Consola import UI
from Validare.Validari import ValidatorPersoana, ValidatorEveniment, ValidatorInscriere


class CoordonatorAplicatie:
    def __init__(self):
        pass

    @staticmethod
    def start():
        while True:
            metoda_de_start = input("Alegeti metoda de salvare a datelor : [Fisier] sau [Memorie] : ")
            if metoda_de_start == "Fisier":
                repository_persoana = PersoanaFileRepository("persoane.txt")
                repository_eveniment = EvenimentFileRepository("evenimente.txt")
                repository_inscriere = InscriereFileRepository("inscrieri.txt")
            elif metoda_de_start == "Memorie":
                repository_persoana = RepositoryPersoana()
                repository_eveniment = RepositoryEveniment()
                repository_inscriere = RepositoryInscriere()
            else:
                continue
            validator_persoana = ValidatorPersoana()
            validator_eveniment = ValidatorEveniment()
            validator_inscriere = ValidatorInscriere()
            service_persoana = ServicePersoana(repository_persoana, validator_persoana)
            service_eveniment = ServiceEveniment(repository_eveniment, validator_eveniment)
            service_inscriere = ServiceInscriere(repository_persoana, repository_eveniment, repository_inscriere, validator_inscriere)
            console = UI(service_persoana, service_eveniment, service_inscriere)
            console.run()
            return
