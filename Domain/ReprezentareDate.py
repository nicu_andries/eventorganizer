class Persoana:
    """
    Clasa persoana, creata un obiect de tip persoana
    Returneaza ID-ul persoanei, numele si adresa
    """

    def __init__(self, person_id, nume, adresa):
        self.__person_id = person_id
        self.__nume = nume
        self.__adresa = adresa

    def get_person_id(self):
        return self.__person_id

    def get_nume(self):
        return self.__nume

    def get_adresa(self):
        return self.__adresa

    def set_person_id(self, person_id):
        self.__person_id = person_id

    def set_nume(self, nume):
        self.__nume = nume

    def set_adresa(self, adresa):
        self.__adresa = adresa

    def __str__(self):
        return str(self.__person_id) + " " + str(self.__nume) + " " + str(self.__adresa)

    def __eq__(self, other):
        return self.__person_id == other.__person_id


class Eveniment:
    """
    Clasa evenimente creaza un obiect de tip evenimente
    Returneaza ID-ul evenimentului, data, timpul si descrierea acestuia
    """

    def __init__(self, eveniment_id, data, timp, descriere):
        self.__eveniment_id = eveniment_id
        self.__data = data
        self.__timp = timp
        self.__descriere = descriere

    def get_eveniment_id(self):
        return self.__eveniment_id

    def get_data(self):
        return self.__data

    def get_timp(self):
        return self.__timp

    def get_descriere(self):
        return self.__descriere

    def set_data(self, data):
        self.__data = data

    def set_timp(self, timp):
        self.__timp = timp

    def set_descriere(self, descriere):
        self.__descriere = descriere

    def __str__(self):
        return self.__eveniment_id + " " + str(self.__data) + " " + self.__timp + " " + self.__descriere

    def __eq__(self, other):
        return self.__eveniment_id == other.__eveniment_id


class Inscriere:
    """
    Clasa de inscriere
    """

    def __init__(self, inscriere_id, persoana, eveniment):
        self.__inscriere_id = inscriere_id
        self.__persoana = persoana
        self.__eveniment = eveniment

    def get_inscriere_id(self):
        return self.__inscriere_id

    def get_persoana(self):
        return self.__persoana

    def get_eveniment(self):
        return self.__eveniment


class InscriereDTO:
    """
    Clasa Inscriere(Data Transfer Object)
    """

    def __init__(self, inscriere_id, person_id, nume, eveniment_id):
        self.__inscriere_id = inscriere_id
        self.__person_id = person_id
        self.__nume = nume
        self.__eveniment_id = eveniment_id

    def __str__(self):
        return str(self.__inscriere_id) + " -- " + str(self.__person_id) + " " + str(self.__nume) + " -- " + str(self.__eveniment_id)
