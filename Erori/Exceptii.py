class ValidatorExceptie(Exception):
    pass


class RepositoryExceptie(Exception):
    pass


class ServiceExceptie(Exception):
    pass


class DuplicatedIDException(Exception):
    pass

