from Erori.Exceptii import RepositoryExceptie


class RepositoryPersoana:
    """
    RepositoryPersoana contine lista unde vor fi stocate toate persoanele +
    functiile de cautare, modificare, etc.. a unei persoane in lista
    """

    def __init__(self):
        self.__elemente = []

    def __len__(self):
        return len(self.__elemente)

    def store(self, persoana):
        if persoana in self.__elemente:
            raise RepositoryExceptie("Exista deja o persoana cu aceleasi date!\n")
        self.__elemente.append(persoana)

    def update(self, person_id_vechi, persoana):
        pozitie = -1
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i].get_person_id() == person_id_vechi:
                pozitie = i
        if pozitie == -1:
            raise RepositoryExceptie("Persoana cautata nu se afla in lista de persoane!\n")
        for person in self.__elemente:
            if person == persoana:
                raise RepositoryExceptie("Persoana introdusa deja se afla in lista de persoane!\n")
        del self.__elemente[pozitie]
        self.__elemente.append(persoana)

    def remove(self, person_id):
        exista = False
        for persoana in self.__elemente:
            if persoana.get_person_id() == person_id:
                exista = True
        if not exista:
            raise RepositoryExceptie("Persoana cautata nu se afla in lista de persoane!\n")
        pozitie = 0
        while pozitie < len(self.__elemente):
            if self.__elemente[pozitie].get_person_id() == person_id:
                self.__elemente.pop(pozitie)
            else:
                pozitie = pozitie + 1

    def get_person_by_id_recursive(self, persoane, person_id):
        if not persoane:
            raise RepositoryExceptie("Persoana cautata nu se afla in lista de persoane!")
        elif persoane[0].get_person_id() == person_id:
            return persoane[0]
        else:
            return self.get_person_by_id_recursive(persoane[1:], person_id)

    def get_person_by_id(self, person_id):
        exista = False
        for persoana in self.__elemente:
            if persoana.get_person_id() == person_id:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista nici o persoana cu ID-ul introdus!\n")
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i].get_person_id() == person_id:
                return self.__elemente[i]

    def search_by_adresa(self, persoane, adresa):
        if not persoane:
            return []
        if persoane[0].get_adresa() != adresa:
            return self.search_by_adresa(persoane[1:], adresa)
        return [persoane[0]] + self.search_by_adresa(persoane[1:], adresa)

    def search_by_adress(self, adresa):
        lista_adresa = []
        exista = False
        for persoana in self.__elemente:
            if persoana.get_adresa() == adresa:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista persoane care locuiesc pe aceasta adresa!\n")
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i].get_adresa() == adresa:
                lista_adresa.append(self.__elemente[i])
        return lista_adresa

    def search_by_first_letter(self, litera):
        lista_litera = []
        exista = False
        for persoana in self.__elemente:
            if (persoana.get_nume())[0] == litera:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista persoane a caror nume incep cu litera introdusa!\n")
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i].get_nume()[0] == litera:
                lista_litera.append(self.__elemente[i])
        return lista_litera

    def get_all(self):
        return self.__elemente[:]


class RepositoryEveniment:
    """
    RepositoryEveniment contine lista unde vor fi stocate toate evenimentele +
    functiile de cautare, modificare, etc.. a unui eveniment in lista
    """

    def __init__(self):
        self.__elemente = []

    def __len__(self):
        return len(self.__elemente)

    def store(self, eveniment):
        if eveniment in self.__elemente:
            raise RepositoryExceptie("Exista deja un eveniment cu aceleasi date!\n")
        self.__elemente.append(eveniment)

    def update(self, eveniment_id_vechi, eveniment):
        pozitie = -1
        lista_evenimente = self.__elemente
        for i in range(0, len(lista_evenimente)):
            if lista_evenimente[i].get_eveniment_id() == eveniment_id_vechi:
                pozitie = i
        if pozitie == -1:
            raise RepositoryExceptie("Evenimentul cautat nu se afla in lista de evenimente!\n")
        for event in lista_evenimente:
            if event == eveniment:
                raise RepositoryExceptie("Evenimentul introdus deja se afla in lista de evenimente!\n")
        del lista_evenimente[pozitie]
        lista_evenimente.append(eveniment)

    def remove(self, eveniment_id):
        exista = False
        for eveniment in self.__elemente:
            if eveniment.get_eveniment_id() == eveniment_id:
                exista = True
        if not exista:
            raise RepositoryExceptie("Evenimentul cautat nu se afla in lista de evenimente!\n")
        pozitie = 0
        while pozitie < len(self.__elemente):
            if self.__elemente[pozitie].get_eveniment_id() == eveniment_id:
                self.__elemente.pop(pozitie)
            else:
                pozitie = pozitie + 1

    def search_by_data(self, data):
        lista_data = []
        exista = False
        for eveniment in self.__elemente:
            if eveniment.get_data() == data:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista nici un eveniment in ziua introdusa!\n")
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i].get_data() == data:
                lista_data.append(self.__elemente[i])
        return lista_data

    def get_eveniment_by_id(self, eveniment_id):
        exista = False
        for eveniment in self.__elemente:
            if eveniment.get_eveniment_id() == eveniment_id:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista nici un eveniment cu ID-ul introdus!\n")
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i].get_eveniment_id() == eveniment_id:
                return self.__elemente[i]

    def search_by_description(self, descriere):
        lista_descriere = []
        exista = False
        for eveniment in self.__elemente:
            if eveniment.get_descriere() == descriere:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista persoane evenimente de acest tip!\n")
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i].get_descriere() == descriere:
                lista_descriere.append(self.__elemente[i])
        return lista_descriere

    def get_all(self):
        return self.__elemente[:]


class RepositoryInscriere:
    """
    RepositoryInscriere contine lista unde vor fi stocate toate inscrierile +
    functiile de cautare, modificare, etc.. a unei inscrieri in lista
    """

    def __init__(self):
        self.__elemente = []

    def __len__(self):
        return len(self.__elemente)

    def adaugare(self, inscriere):
        """
        Metoda de adaugare a unei inscrieri
        """
        for i in range(0, len(self.__elemente)):
            if inscriere.get_inscriere_id == self.__elemente[i][1]:
                raise RepositoryExceptie("Inscriere existenta!")
            if inscriere.get_persoana().get_person_id() == self.__elemente[i][1] and inscriere.get_eveniment().get_eveniment_id() == self.__elemente[i][2]:
                raise RepositoryExceptie("Inscriere existenta!")
        self.__elemente.append([inscriere.get_inscriere_id(), inscriere.get_persoana().get_person_id(), inscriere.get_eveniment().get_eveniment_id()])

    def remove_inscriere_by_id(self, inscriere_id):
        """
        Metoda de stergere a unei inscrieri dupa ID
        """
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i][0] == inscriere_id:
                del self.__elemente[i]
                return
        raise RepositoryExceptie("Inscriere inexistenta!")

    def update_inscriere_persoana_by_id(self, inscriere_id, person_id):
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i][0] == inscriere_id:
                rezultat = [self.__elemente[i][0], person_id, self.__elemente[i][2]]
                del self.__elemente[i]
                self.__elemente.append(rezultat)
                return
        raise RepositoryExceptie("Inscriere inexistenta!")

    def update_inscriere_eveniment_by_id(self, inscriere_id, eveniment_id):
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i][0] == inscriere_id:
                rezultat = [self.__elemente[i][0], self.__elemente[i][1], eveniment_id]
                del self.__elemente[i]
                self.__elemente.append(rezultat)
                return
        raise RepositoryExceptie("Inscriere inexistenta!")

    def lista_evenimente_persoana(self, person_id):
        """
        Metoda de salvat evenimentele la care participa o persoana
        """
        lista_rezultat = []
        exista = False
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i][1] == person_id:
                exista = True
        if not exista:
            raise RepositoryExceptie("Persoana introdusa nu este inscrisa la nici un eveniment!\n")
        for i in range(0, len(self.__elemente)):
            if self.__elemente[i][1] == person_id:
                eveniment = self.__elemente[i][2]
                lista_rezultat.append(eveniment)
        return lista_rezultat

    def get_all(self):
        return self.__elemente[:]
