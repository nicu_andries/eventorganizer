from Domain.ReprezentareDate import Persoana, Eveniment
from Erori.Exceptii import RepositoryExceptie
from Infastructure.Repositories import RepositoryPersoana, RepositoryEveniment, RepositoryInscriere


class PersoanaFileRepository(RepositoryPersoana):
    """
    Stocare/Extragere persoane din fisier
    """

    def __init__(self, nume_fisier):
        """
        Initializare repository
        nume_fisier - string, calea fisierului unde sunt stocate persoanele
        """
        self.__nume_fisier = nume_fisier
        RepositoryPersoana.__init__(self)

    def __citeste_persoana_din_fisier(self):
        with open(self.__nume_fisier, "r") as fisier:
            lines = fisier.readlines()
            self.__elemente = []
            for linie in lines:
                linie = linie.strip()
                if linie != "":
                    parametri = linie.split(";")
                    persoana = Persoana(parametri[0], parametri[1], parametri[2])
                    self.__elemente.append(persoana)
        return self.__elemente

    def __adauga_persoana_in_fisier(self, persoana):
        with open(self.__nume_fisier, "a") as fisier:
            fisier.write(str(persoana.get_person_id()) + ";" + str(persoana.get_nume()) + ";" + str(persoana.get_adresa()) + ";\n")

    def __scrie_in_fisier(self, persoane):
        with open(self.__nume_fisier, "w") as fisier:
            for persoana in persoane:
                fisier.write(str(persoana.get_person_id()) + ";" + str(persoana.get_nume()) + ";" + str(persoana.get_adresa()) + ";\n")

    def store(self, persoana):
        persoane = self.__citeste_persoana_din_fisier()
        if persoana in persoane:
            raise RepositoryExceptie("Exista deja o persoana cu acelasi ID!")
        RepositoryPersoana.store(self, persoana)
        self.__adauga_persoana_in_fisier(persoana)

    def remove(self, person_id):
        all_persoane = self.__citeste_persoana_din_fisier()
        pozitie = -1
        for i in range(0, len(all_persoane)):
            if all_persoane[i].get_person_id() == person_id:
                pozitie = i
        if pozitie == -1:
            raise RepositoryExceptie("Nu exista nici o persoana cu ID-ul cautat!")
        del all_persoane[pozitie]
        self.__scrie_in_fisier(all_persoane)

    def get_person_by_id_recursive(self, persoane, person_id):
        if not persoane:
            raise RepositoryExceptie("Persoana cautata nu se afla in lista de persoane!")
        elif persoane[0].get_person_id() == person_id:
            return persoane[0]
        else:
            return self.get_person_by_id_recursive(persoane[1:], person_id)

    def search_by_adresa(self, persoane, adresa):
        if not persoane:
            return []
        if persoane[0].get_adresa() != adresa:
            return self.search_by_adresa(persoane[1:], adresa)
        return [persoane[0]] + self.search_by_adresa(persoane[1:], adresa)

    def search_by_adress(self, adresa):
        lista_adresa = []
        persoane = self.__citeste_persoana_din_fisier()
        exista = False
        for persoana in persoane:
            if persoana.get_adresa() == adresa:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista persoane care locuiesc pe aceasta adresa!\n")
        for i in range(0, len(persoane)):
            if persoane[i].get_adresa() == adresa:
                lista_adresa.append(persoane[i])
        return lista_adresa

    def search_by_first_letter(self, litera):
        lista_litera = []
        persoane = self.__citeste_persoana_din_fisier()
        exista = False
        for persoana in persoane:
            if (persoana.get_nume())[0] == litera:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista persoane a caror nume incep cu litera introdusa!\n")
        for i in range(0, len(persoane)):
            if persoane[i].get_nume()[0] == litera:
                lista_litera.append(persoane[i])
        return lista_litera

    def get_person_by_id(self, person_id):
        persoane = self.__citeste_persoana_din_fisier()
        for persoana in persoane:
            if persoana.get_person_id() == person_id:
                return persoana
        raise RepositoryExceptie("Persoana cautata nu se afla in lista de persoane!")

    """
    - Caz favorabil - elementul cautat este pe prima pozitie
    T(n) = 1  θ(1)
    
    - Caz defavorabil - elementul cautat nu este in lista
    T(n) = n θ(n)
    
    - Caz mediu - se itereaza de 1..n ori
    T(n) = (1 + 2 + .. + n)/n = n(n+1)/2n = (n+1)/2 = θ(n)
    
    Complexitate:
    O(n)
    """

    def update(self, person_id_vechi, persoana):
        pozitie = -1
        lista_persoane = self.__citeste_persoana_din_fisier()
        for i in range(0, len(lista_persoane)):
            if lista_persoane[i].get_person_id() == person_id_vechi:
                pozitie = i
        if pozitie == -1:
            raise RepositoryExceptie("Persoana cautata nu se afla in lista de persoane!\n")
        for person in lista_persoane:
            if person == persoana:
                raise RepositoryExceptie("Persoana introdusa deja se afla in lista de persoane!\n")
        del lista_persoane[pozitie]
        lista_persoane.append(persoana)
        self.__scrie_in_fisier(lista_persoane)

    def remove_all(self):
        return self.__scrie_in_fisier([])

    def get_all(self):
        return self.__citeste_persoana_din_fisier()

    def size(self):
        return len(self.__citeste_persoana_din_fisier())

    def __len__(self):
        self.__citeste_persoana_din_fisier()
        return RepositoryPersoana.__len__(self)


class EvenimentFileRepository(RepositoryEveniment):
    def __init__(self, nume_fisier):
        self.__nume_fisier = nume_fisier
        RepositoryEveniment.__init__(self)

    def __citeste_eveniment_din_fisier(self):
        with open(self.__nume_fisier, "r") as fisier:
            lines = fisier.readlines()
            self.__elemente = []
            for linie in lines:
                linie = linie.strip()
                if linie != "":
                    parametri = linie.split(";")
                    eveniment = Eveniment(parametri[0], parametri[1], parametri[2], parametri[3])
                    self.__elemente.append(eveniment)
        return self.__elemente

    def __adauga_eveniment_in_fisier(self, eveniment):
        with open(self.__nume_fisier, "a") as fisier:
            fisier.write(str(eveniment.get_eveniment_id()) + ";" + str(eveniment.get_data()) + ";" + str(eveniment.get_timp()) + ";" + str(eveniment.get_descriere()) + ";\n")

    def __scrie_in_fisier(self, evenimente):
        with open(self.__nume_fisier, "w") as fisier:
            for eveniment in evenimente:
                fisier.write(str(eveniment.get_eveniment_id()) + ";" + str(eveniment.get_data()) + ";" + str(eveniment.get_timp()) + ";" + str(eveniment.get_descriere()) + ";\n")

    def store(self, eveniment):
        evenimente = self.__citeste_eveniment_din_fisier()
        if eveniment in evenimente:
            raise RepositoryExceptie("Exista deja un eveniment cu acest ID!")
        RepositoryEveniment.store(self, eveniment)
        self.__adauga_eveniment_in_fisier(eveniment)

    def remove(self, eveniment_id):
        all_evenimente = self.__citeste_eveniment_din_fisier()
        pozitie = -1
        for i in range(0, len(all_evenimente)):
            if all_evenimente[i].get_eveniment_id() == eveniment_id:
                pozitie = i
        if pozitie == -1:
            raise RepositoryExceptie("Nu exista nici un eveniment cu ID-ul cautat!")
        del all_evenimente[pozitie]
        self.__scrie_in_fisier(all_evenimente)

    def update(self, eveniment_id_vechi, eveniment):
        pozitie = -1
        lista_evenimente = self.__citeste_eveniment_din_fisier()
        for i in range(0, len(lista_evenimente)):
            if lista_evenimente[i].get_eveniment_id() == eveniment_id_vechi:
                pozitie = i
        if pozitie == -1:
            raise RepositoryExceptie("Evenimentul cautat nu se afla in lista de evenimente!\n")
        for event in lista_evenimente:
            if event == eveniment:
                raise RepositoryExceptie("Persoana introdusa deja se afla in lista de persoane!\n")
        del lista_evenimente[pozitie]
        lista_evenimente.append(eveniment)
        self.__scrie_in_fisier(lista_evenimente)

    def get_eveniment_by_id(self, eveniment_id):
        evenimente = self.__citeste_eveniment_din_fisier()
        for eveniment in evenimente:
            if eveniment.get_eveniment_id() == eveniment_id:
                return eveniment
        raise RepositoryExceptie("Evenimentul cautat nu se afla in lista de evenimente!")

    def search_by_data(self, data):
        lista_data = []
        evenimente = self.__citeste_eveniment_din_fisier()
        exista = False
        for eveniment in evenimente:
            if eveniment.get_data() == data:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista nici un eveniment in ziua introdusa!\n")
        for i in range(0, len(evenimente)):
            if evenimente[i].get_data() == data:
                lista_data.append(evenimente[i])
        return lista_data

    def search_by_description(self, descriere):
        lista_descriere = []
        evenimente = self.__citeste_eveniment_din_fisier()
        exista = False
        for eveniment in evenimente:
            if eveniment.get_descriere() == descriere:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista persoane evenimente de acest tip!\n")
        for i in range(0, len(evenimente)):
            if evenimente[i].get_descriere() == descriere:
                lista_descriere.append(evenimente[i])
        return lista_descriere

    def remove_all(self):
        return self.__scrie_in_fisier([])

    def get_all(self):
        return self.__citeste_eveniment_din_fisier()

    def size(self):
        return len(self.__citeste_eveniment_din_fisier())

    def __len__(self):
        self.__citeste_eveniment_din_fisier()
        return RepositoryEveniment.__len__(self)


class InscriereFileRepository(RepositoryInscriere):

    def __init__(self, nume_fisier):
        """
        Initializare repository
        nume_fisier - string, calea fisierului unde sunt stocate persoanele
        """
        self.__nume_fisier = nume_fisier
        RepositoryInscriere.__init__(self)

    def __citeste_inscriere_din_fisier(self):
        with open(self.__nume_fisier, "r") as fisier:
            lines = fisier.readlines()
            self.__elemente = []
            for linie in lines:
                linie = linie.strip()
                if linie != "":
                    parametri = linie.split(";")
                    inscriere = [parametri[0], parametri[1], parametri[2]]
                    self.__elemente.append(inscriere)
        return self.__elemente

    def __adauga_inscriere_in_fisier(self, inscriere):
        with open(self.__nume_fisier, "a") as fisier:
            fisier.write(inscriere.get_inscriere_id() + ";" + inscriere.get_persoana().get_person_id() + ";" + inscriere.get_eveniment().get_eveniment_id() + ";\n")

    def __scrie_in_fisier(self, inscrieri):
        with open(self.__nume_fisier, "w") as fisier:
            for inscriere in inscrieri:
                fisier.write(inscriere[0] + ";" + inscriere[1] + ";" + inscriere[2] + ";\n")

    def adaugare(self, inscriere):
        inscrieri = self.__citeste_inscriere_din_fisier()
        for i in range(0, len(inscrieri)):
            if inscriere.get_inscriere_id == inscrieri[i][1]:
                raise RepositoryExceptie("Inscriere existenta!")
            if inscriere.get_persoana().get_person_id() == inscrieri[i][1] and inscriere.get_eveniment().get_eveniment_id() == inscrieri[i][2]:
                raise RepositoryExceptie("Inscriere existenta!")
        RepositoryInscriere.adaugare(self, inscriere)
        self.__adauga_inscriere_in_fisier(inscriere)

    def remove_inscriere_by_id(self, inscriere_id):
        """
        Metoda de stergere a unei inscrieri dupa ID
        """
        lista_inscrieri = self.__citeste_inscriere_din_fisier()
        for i in range(0, len(lista_inscrieri)):
            if lista_inscrieri[i][0] == inscriere_id:
                del lista_inscrieri[i]
                self.__scrie_in_fisier(lista_inscrieri)
                return
        raise RepositoryExceptie("Inscriere inexistenta!")

    def update_inscriere_persoana_by_id(self, inscriere_id, person_id):
        lista_inscrieri = self.__citeste_inscriere_din_fisier()
        for i in range(0, len(lista_inscrieri)):
            if lista_inscrieri[i][0] == inscriere_id:
                rezultat = [lista_inscrieri[i][0], person_id, lista_inscrieri[i][2]]
                del lista_inscrieri[i]
                lista_inscrieri.append(rezultat)
                self.__scrie_in_fisier(lista_inscrieri)
                return
        raise RepositoryExceptie("Inscriere inexistenta!")

    def update_inscriere_eveniment_by_id(self, inscriere_id, eveniment_id):
        lista_inscrieri = self.__citeste_inscriere_din_fisier()
        for i in range(0, len(lista_inscrieri)):
            if lista_inscrieri[i][0] == inscriere_id:
                rezultat = [lista_inscrieri[i][0], lista_inscrieri[i][1], eveniment_id]
                del lista_inscrieri[i]
                lista_inscrieri.append(rezultat)
                self.__scrie_in_fisier(lista_inscrieri)
                return
        raise RepositoryExceptie("Inscriere inexistenta!")

    def lista_evenimente_persoana(self, person_id):
        """
        Metoda de salvat evenimentele la care participa o persoana
        """
        lista_rezultat = []
        lista_inscrieri = self.__citeste_inscriere_din_fisier()
        exista = False
        for i in range(0, len(lista_inscrieri)):
            if lista_inscrieri[i][1] == person_id:
                exista = True
        if not exista:
            raise RepositoryExceptie("Persoana introdusa nu este inscrisa la nici un eveniment!\n")
        for i in range(0, len(lista_inscrieri)):
            if lista_inscrieri[i][1] == person_id:
                lista_rezultat.append(lista_inscrieri[i][2])
        return lista_rezultat

    def get_all(self):
        return self.__citeste_inscriere_din_fisier()

    def __len__(self):
        self.__citeste_inscriere_din_fisier()
        return RepositoryInscriere.__len__(self)
