from Domain.ReprezentareDate import Persoana
from Erori.Exceptii import RepositoryExceptie


class PersoanaOnlyFileRepository:
    """
    Stocare/Extragere persoane din fisier
    """

    def __init__(self, nume_fisier):
        """
        Initializare repository
        nume_fisier - string, calea fisierului unde sunt stocate persoanele
        """
        self.__nume_fisier = nume_fisier

    def store(self, persoana):
        with open(self.__nume_fisier, "r") as fisier:
            for line in fisier:
                if line.strip().startswith(str(persoana.get_person_id())):
                    raise RepositoryExceptie("Persoana este deja in lista de persoane!")
        with open(self.__nume_fisier, "a") as fisier:
            data_to_store = persoana.get_person_id() + ";" + persoana.get_nume() + ";" + persoana.get_adresa() + ";"
            fisier.write(f"{data_to_store}\n")
        with open("fisier_auxiliar.txt", "a") as fisier_auxiliar:
            fisier_auxiliar.write(f"{data_to_store}\n")

    def update(self, person_id_vechi, persoana):
        self.get_person_by_id(person_id_vechi)
        fisier = open(self.__nume_fisier, "r")
        fisier_auxiliar = open("fisier_auxiliar.txt", "w")
        try:
            while True:
                line = fisier.readline()
                if not line.strip().startswith(str(person_id_vechi)):
                    fisier_auxiliar.write(line.strip() + "\n")
                if not line:
                    break
        finally:
            fisier.close()
            fisier_auxiliar.close()
        fisier = open(self.__nume_fisier, "w")
        fisier_auxiliar = open("fisier_auxiliar.txt", "r")
        try:
            while True:
                line = fisier_auxiliar.readline()
                fisier.write(line.strip() + "\n")
                if not line:
                    break
            data_to_store = persoana.get_person_id() + ";" + persoana.get_nume() + ";" + persoana.get_adresa() + ";"
            fisier.write(f"{data_to_store}\n")
        finally:
            fisier.close()
            fisier_auxiliar.close()

    def remove(self, person_id):
        fisier = open(self.__nume_fisier, "r")
        self.get_person_by_id(person_id)
        fisier_auxiliar = open("fisier_auxiliar.txt", "w")
        try:
            while True:
                line = fisier.readline()
                if not line.strip().startswith(str(person_id)):
                    fisier_auxiliar.write(line.strip() + "\n")
                if not line:
                    break
        finally:
            fisier.close()
            fisier_auxiliar.close()
        fisier = open(self.__nume_fisier, "w")
        fisier_auxiliar = open("fisier_auxiliar.txt", "r")
        try:
            while True:
                line = fisier_auxiliar.readline()
                fisier.write(line.strip() + "\n")
                if not line:
                    break
        finally:
            fisier.close()
            fisier_auxiliar.close()

    def search_by_adress(self, adresa):
        lista_adresa = []
        persoane = self.get_all()
        exista = False
        for persoana in persoane:
            if persoana.get_adresa() == adresa:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista persoane care locuiesc pe aceasta adresa!\n")
        for i in range(0, len(persoane)):
            if persoane[i].get_adresa() == adresa:
                lista_adresa.append(persoane[i])
        return lista_adresa

    def search_by_first_letter(self, litera):
        lista_litera = []
        persoane = self.get_all()
        exista = False
        for persoana in persoane:
            if (persoana.get_nume())[0] == litera:
                exista = True
        if not exista:
            raise RepositoryExceptie("Nu exista persoane a caror nume incep cu litera introdusa!\n")
        for i in range(0, len(persoane)):
            if persoane[i].get_nume()[0] == litera:
                lista_litera.append(persoane[i])
        return lista_litera

    def get_person_by_id_recursive(self, persoane, person_id):
        if not persoane:
            raise RepositoryExceptie("Persoana cautata nu se afla in lista de persoane!")
        elif persoane[0].get_person_id() == person_id:
            return persoane[0]
        else:
            return self.get_person_by_id_recursive(persoane[1:], person_id)

    def get_all(self):
        with open(self.__nume_fisier, "r") as fisier:
            lines = fisier.readlines()
            lista_persoane = []
            for linie in lines:
                linie = linie.strip()
                if linie != "":
                    parametri = linie.split(";")
                    persoana = Persoana(parametri[0], parametri[1], parametri[2])
                    lista_persoane.append(persoana)
        return lista_persoane

    def get_person_by_id(self, person_id):
        with open(self.__nume_fisier, "r") as fisier:
            for line in fisier:
                if line.strip().startswith(str(person_id)):
                    parametri = line.split(";")
                    persoana = Persoana(parametri[0], parametri[1], parametri[2])
                    return persoana
            raise RepositoryExceptie("Nu exista nici o persoana cu ID-ul introdus!")
