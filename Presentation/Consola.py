from Domain.ReprezentareDate import Persoana, Eveniment
from Erori.Exceptii import RepositoryExceptie, ValidatorExceptie, ServiceExceptie


class UI:

    def __ui_adaugare_persoana(self):
        try:
            person_id = input("Introduceti ID-ul persoanei [4 cifre]: ")
            nume = input("Introduceti numele persoanei fiecare incepand cu litera mare : ")
            adresa = input("Introduceti adresa persoanei [str. Adresa nr.__] : ")
            self.__service_persoana.adaugare_persoana(person_id, nume, adresa)
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_adaugare_persona_random(self):
        try:
            numar_persoane = int(input("Introduceti numarul de persoane pe care doriti sa le generati : "))
            self.__service_persoana.adaugare_persoana_random(numar_persoane)
        except TypeError:
            print("Numarul de persoane trebuie sa fie un intreg!")
        except ValidatorExceptie as eroare_validare:
            print(eroare_validare)
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)
        except ServiceExceptie as eroare_service:
            print(eroare_service)
        except ValueError:
            print("Eroare!")

    def __ui_adaugare_eveniment_random(self):
        try:
            numar_evenimente = int(input("Introduceti numarul de evenimente pe care doriti sa le generati : "))
            self.__service_eveniment.adaugare_eveniment_random(numar_evenimente)
        except TypeError:
            print("Numarul de evenimente trebuie sa fie un intreg!")
        except ValidatorExceptie as eroare_validare:
            print(eroare_validare)
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)
        except ServiceExceptie as eroare_service:
            print(eroare_service)
        except ValueError:
            print("Eroare!")

    def __ui_adaugare_eveniment(self):
        try:
            eveniment_id = input("Introduceti ID-ul evenimentului [Litera mare + 3 cifre] : ")
            data = input("Introduceti data cand va avea loc evenimentul [aaaa.ll.zz] : ")
            timp = input("Introduceti ora la care va avea loc evenimentul [ora.minute] : ")
            descriere = input("Introduceti tipul evenimentului : ")
            self.__service_eveniment.adaugare_eveniment(eveniment_id, data, timp, descriere)
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_modificare_persoana(self):
        try:
            person_id_vechi = input("Introduceti ID-ul persoanei care trebuie modificata [4 cifre] : ")
            person_id = input("Introduceti ID-ul corect al persoanei [4 cifre] : ")
            nume = input("Introduceti numele persoanei fiecare incepand cu litera mare : ")
            adresa = input("Introduceti adresa persoanei [str. Adresa nr.__] : ")
            persoana = Persoana(person_id, nume, adresa)
            self.__service_inscriere.modificare_persoana(person_id_vechi, persoana)
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_modificare_eveniment(self):
        try:
            eveniment_id_vechi = input("Introduceti ID-ul evenimentului care trebuie modificat [Litera mare + 3 cifre] : ")
            eveniment_id = input("Introduceti ID-ul corect al evenimentului [Litera mare + 3 cifre]: ")
            data = input("Introduceti data cand va avea loc evenimentul [aaaa.ll.zz] : ")
            timp = input("Introduceti ora la care va avea loc evenimentul [ora.minute] : ")
            descriere = input("Introduceti tipul evenimentului : ")
            eveniment = Eveniment(eveniment_id, data, timp, descriere)
            self.__service_inscriere.modificare_eveniment(eveniment_id_vechi, eveniment)
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_afisare_persoane(self):
        persoane = self.__service_persoana.get_persoane()
        if len(persoane) == 0:
            print("Nu exista persoane!\n")
            return
        print("Lista de persoane este : ")
        for persoana in persoane:
            print(persoana)

    def __ui_afisare_evenimente(self):
        evenimente = self.__service_eveniment.get_evenimente()
        if len(evenimente) == 0:
            print("Nu exista evenimente!\n")
            return
        print("Lista de evenimente este : ")
        for eveniment in evenimente:
            print(eveniment)

    def __ui_stergere_persoana(self):
        try:
            person_id = input("Introduceti ID-ul persoanei care urmeaza a fi stearsa [4 cifre] : ")
            self.__service_inscriere.remove_inscriere_si_persoana_by_id(person_id)
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_stergere_eveniment(self):
        try:
            eveniment_id = input("Introduceti ID-ul evenimentului care urmeaza a fi sters [Litera mare + 3 cifre] : ")
            self.__service_inscriere.remove_inscriere_si_evenimente_by_id(eveniment_id)
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_cautare_persoane_adresa(self):
        try:
            adresa = input("Introduceti adresa pentru care vor fi afisate persoanele [str. Adresa nr.__] : ")
            lista = self.__service_persoana.cautare_dupa_adresa(adresa)
            for i in range(0, len(lista)):
                print(lista[i])
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_cautare_persoane_litera(self):
        try:
            litera = input("Introduceti litera pentru care vor fi afisate persoanele cu numele incepand cu litera introdusa : ")
            lista = self.__service_persoana.cautare_dupa_litera(litera)
            for i in range(0, len(lista)):
                print(lista[i])
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_cautare_eveniment_data(self):
        try:
            data = input("Introduceti data pentru care doriti sa fie afisate evenimentele [aaaa.ll.zz] : ")
            lista = self.__service_eveniment.cautare_dupa_data(data)
            for i in range(0, len(lista)):
                print(lista[i])
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_cautare_eveniment_descriere(self):
        try:
            descriere = input("Introduceti descrierea pentru care doriti sa fie afisate evenimentele : ")
            lista = self.__service_eveniment.cautare_dupa_descriere(descriere)
            for i in range(0, len(lista)):
                print(lista[i])
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_inscriere_persoana_eveniment(self):
        try:
            person_id = input("Introduceti ID-ul persoanei care urmeaza a fi inscrisa la eveniment [4 cifre] : ")
            eveniment_id = input("Introduceti ID-ul evenimentului la care urmeaza a fi inscrisa persoana [Litera mare + 3 cifre] : ")
            self.__service_inscriere.adaugare_inscriere(person_id, eveniment_id)
        except RepositoryExceptie as eroare_repository:
            print(eroare_repository)

    def __ui_afisare_inscrieri_persoana(self):
        inscrieri = self.__service_inscriere.get_inscrieri_nou()
        if not inscrieri:
            print("Nici o persoana nu este inscrisa la nici un eveniment!")
            return
        print("Persoanele inscrise la evenimente sunt : ")
        for i in range(0, len(inscrieri)):
            print(inscrieri[i].get_inscriere_id(), "--", str(inscrieri[i].get_persoana()), "--", str(inscrieri[i].get_eveniment()))

    def __ui_afisare_evenimente_persoana_sortat_data(self):
        person_id = input("Introduceti ID-ul persoanei pentru care doriti sa afisati evenimentele : ")
        evenimente = self.__service_inscriere.afisare_evenimente_persoana_sortat_data(person_id)
        if not evenimente:
            print("Persoana nu este inscrisa la nici un eveniment!")
            return
        print(f"Evenimentele la care este inscrisa persoana cu ID-ul [{person_id}] sunt : ")
        for i in range(0, len(evenimente)):
            print(str(evenimente[i]))

    def __ui_afisare_evenimente_persoana_sortat_descriere(self):
        person_id = input("Introduceti ID-ul persoanei pentru care doriti sa afisati evenimentele : ")
        evenimente = self.__service_inscriere.afisare_evenimente_persoana_sortat_descriere(person_id)
        if not evenimente:
            print("Persoana nu este inscrisa la nici un eveniment!")
            return
        print(f"Evenimentele la care este inscrisa persoana cu ID-ul [{person_id}] sunt : ")
        for i in range(0, len(evenimente)):
            print(str(evenimente[i]))

    def __ui_afisare_persoana_numar_evenimente_maxim(self):
        rezultat = self.__service_inscriere.numarul_de_evenimente_persoana()
        if not rezultat:
            print("Nici o persoana nu este inscrisa la vreun eveniment!")
            return
        for i in range(0, len(rezultat)):
            print(rezultat[i][0], "-- Evenimente :", rezultat[i][1])

    def __ui_afisare_evenimente_numar_persoane_maxim(self):
        try:
            rezultat = self.__service_inscriere.numarul_de_persoane_eveniment()
            if not rezultat:
                print("Nici o persoana nu este inscrisa la vreun eveniment!")
                return
            for i in range(0, len(rezultat)):
                print(rezultat[i][0].get_descriere(), "-- Persoane :", rezultat[i][1])
        except ServiceExceptie as eroare_service:
            print(eroare_service)

    def __ui_afisare_persoane_evenimente_zi(self):
        try:
            data = input("Introduceti data cand va avea loc evenimentul [aaaa.ll.zz] : ")
            rezultat = self.__service_inscriere.persoane_participante_evenimente_zi(data)
            if not rezultat:
                print("Nici o persoana nu este inscrisa la vreun eveniment in aceasta zi!")
                return
            for i in range(0, len(rezultat)):
                print("Persoana :", rezultat[i][0].get_person_id(), rezultat[i][0].get_nume(), "-- Evenimente :", rezultat[i][1])
        except RepositoryExceptie as eroare:
            print(eroare)

    def __ui_sortare_persoane_bubble(self):
        persoane = self.__service_persoana.sortare_persoane_bubble()
        if not persoane:
            print("Nu exista nici o persoana introdusa!")
        for persoana in persoane:
            print(persoana)

    def __ui_sortare_persoane_shell(self):
        persoane = self.__service_persoana.sortare_persoane_shell()
        if not persoane:
            print("Nu exista nici o persoana introdusa!")
        for persoana in persoane:
            print(persoana)

    def __ui_sortare_persoane_bubble_doua_chei(self):
        persoane = self.__service_persoana.sortare_persoane_bubble_doua_chei()
        if not persoane:
            print("Nu exista nici o persoana introdusa!")
        for persoana in persoane:
            print(persoana)

    def __init__(self, service_persoana, service_eveniment, service_inscriere):
        self.__service_persoana = service_persoana
        self.__service_eveniment = service_eveniment
        self.__service_inscriere = service_inscriere
        self.__comenzi = {
            "1": self.__ui_adaugare_persoana,
            "2": self.__ui_adaugare_eveniment,
            "3": self.__ui_modificare_persoana,
            "4": self.__ui_modificare_eveniment,
            "5": self.__ui_afisare_persoane,
            "6": self.__ui_afisare_evenimente,
            "7": self.__ui_stergere_persoana,
            "8": self.__ui_stergere_eveniment,
            "9": self.__ui_cautare_persoane_adresa,
            "10": self.__ui_cautare_persoane_litera,
            "11": self.__ui_cautare_eveniment_data,
            "12": self.__ui_cautare_eveniment_descriere,
            "13": self.__ui_inscriere_persoana_eveniment,
            "14": self.__ui_afisare_inscrieri_persoana,
            "15": self.__ui_adaugare_persona_random,
            "16": self.__ui_adaugare_eveniment_random,
            "17": self.__ui_afisare_evenimente_persoana_sortat_data,
            "18": self.__ui_afisare_evenimente_persoana_sortat_descriere,
            "19": self.__ui_afisare_persoana_numar_evenimente_maxim,
            "20": self.__ui_afisare_evenimente_numar_persoane_maxim,
            "21": self.__ui_afisare_persoane_evenimente_zi,
            "22": self.__ui_sortare_persoane_bubble,
            "23": self.__ui_sortare_persoane_bubble_doua_chei,
            "24": self.__ui_sortare_persoane_shell
        }

    def run(self):
        """
        Meniul
        """
        while True:
            print("\nMeniu :")
            print("1. Adaugare persoana!")
            print("2. Adaugare eveniment!")
            print("3. Modificare persoana!")
            print("4. Modificare eveniment!")
            print("5. Afisare persoane!")
            print("6. Afisare evenimente!")
            print("7. Stergere persoana!")
            print("8. Stergere evenimente!")
            print("9. Cautare persoane dupa adresa!")
            print("10. Cautare persoane dupa prima litera a numelui!")
            print("11. Cautare eveniment dupa data!")
            print("12. Cautare eveniment dupa descriere!")
            print("13. Inscriere persoana eveniment!")
            print("14. Afisarea persoanelor si evenimentele la care sunt inscrise!")
            print("15. Adaugare persoane random!")
            print("16. Adaugare evenimente random!")
            print("17. Afisarea evenimentelor pentru o anumita persoana ordonate dupa data!")
            print("18. Afisarea evenimentelor pentru o anumita persoana ordonate dupa descriere!")
            print("19. Afisarea persoanelor care participa la cele mai multe evenimente!")
            print("20. Afisarea primelor 20% de evenimente cu cei mai multi participanti!")
            print("21. Lista persoanelor care merg la evenimente intr-o zi! ")
            print("22. Afisarea listei de persoane sortate utilizand metoda BubbleSort!")
            print("23. Afisarea listei de persoane sortate utilizand metoda BubbleSort dupa 2 chei!")
            print("24. Afisarea listei de persoane sortate utilizand metoda ShellSort!")
            print("25. Iesire!\n")
            comanda = input("Alegeti operatia pe care doriti sa o efectuati : ")
            if comanda == "25":
                print("Ati iesit din aplicatie!")
                return
            if comanda in self.__comenzi:
                try:
                    self.__comenzi[comanda]()
                except ValueError:
                    print("Valoare numerica invalida!\n")
                except ValidatorExceptie as eroare_validare:
                    print(eroare_validare)
                except RepositoryExceptie as eroare_repository:
                    print(eroare_repository)
            else:
                print("Comanda invalida!\n")
