def Bubble_sort(lista, key=lambda cheie: cheie, reverse=False):
    if not reverse:
        for i in range(1, len(lista)):
            for j in range(0, len(lista) - i):
                if key(lista[j]) > key(lista[j + 1]):
                    lista[j], lista[j + 1] = lista[j + 1], lista[j]
    elif reverse:
        for i in range(1, len(lista)):
            for j in range(0, len(lista) - i):
                if key(lista[j]) < key(lista[j + 1]):
                    lista[j], lista[j + 1] = lista[j + 1], lista[j]
    return lista


def Shell_sort(lista, key=lambda cheie: cheie, reverse=False):
    if not reverse:
        interval = len(lista) // 2
        while interval > 0:
            for i in range(interval, len(lista)):
                auxiliar = lista[i]
                j = i
                while j >= interval and key(lista[j - interval]) > key(auxiliar):
                    lista[j] = lista[j - interval]
                    j = j - interval
                lista[j] = auxiliar
            interval = interval // 2
    elif reverse:
        interval = len(lista) // 2
        while interval > 0:
            for i in range(interval, len(lista)):
                auxiliar = lista[i]
                j = i
                while j >= interval and key(lista[j - interval]) < key(auxiliar):
                    lista[j] = lista[j - interval]
                    j = j - interval
                lista[j] = auxiliar
            interval = interval // 2
    return lista


def Bubble_sort_doua_chei(lista, key=lambda cheie: cheie, key1=lambda cheie1: cheie1, reverse=False):
    if not reverse:
        for i in range(1, len(lista)):
            for j in range(0, len(lista) - i):
                if key(lista[j]) > key(lista[j + 1]):
                    lista[j], lista[j + 1] = lista[j + 1], lista[j]
                elif (key(lista[j]) == key(lista[j + 1])) and (key1(lista[j]) > key1(lista[j + 1])):
                    lista[j], lista[j + 1] = lista[j + 1], lista[j]
    elif reverse:
        for i in range(1, len(lista)):
            for j in range(0, len(lista) - i):
                if key(lista[j]) < key(lista[j + 1]):
                    lista[j], lista[j + 1] = lista[j + 1], lista[j]
                elif (key(lista[j]) == key(lista[j + 1])) and (key1(lista[j]) < key1(lista[j + 1])):
                    lista[j], lista[j + 1] = lista[j + 1], lista[j]
    return lista
