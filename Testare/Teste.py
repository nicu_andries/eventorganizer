import unittest

from Bussines.ServiceInscriere import ServiceInscriere
from Bussines.ServicesEveniment import ServiceEveniment
from Bussines.ServicesPersoana import ServicePersoana
from Domain.ReprezentareDate import Persoana, Eveniment, Inscriere
from Erori.Exceptii import ValidatorExceptie, RepositoryExceptie, ServiceExceptie
from Infastructure.Repositories import RepositoryPersoana, RepositoryEveniment, RepositoryInscriere
from Infastructure.RepositoryFile import PersoanaFileRepository
from Validare.Validari import ValidatorPersoana, ValidatorEveniment, ValidatorInscriere


class Teste(unittest.TestCase):
    """
    Clasa de testare pentru : Domain, Services, Repositories, Validari
    """

    def test_domain(self):
        person_id = "0001"
        nume = "Nicolae Andries"
        adresa = "str. Independentei nr.14"
        persoana = Persoana(person_id, nume, adresa)
        self.assertEqual(persoana.get_person_id(), "0001")
        self.assertEqual(persoana.get_nume(), "Nicolae Andries")
        self.assertEqual(persoana.get_adresa(), "str. Independentei nr.14")
        self.assertEqual(str(persoana), "0001 Nicolae Andries str. Independentei nr.14")
        persoana1 = Persoana(person_id, None, adresa)
        self.assertEqual(persoana, persoana1)
        eveniment_id = "H123"
        data = "20/12/2020"
        timp = "10:30"
        descriere = "Nunta"
        eveniment = Eveniment(eveniment_id, data, timp, descriere)
        self.assertEqual(eveniment.get_eveniment_id(), "H123")
        self.assertEqual(eveniment.get_data(), "20/12/2020")
        self.assertEqual(eveniment.get_timp(), "10:30")
        self.assertEqual(eveniment.get_descriere(), "Nunta")
        self.assertEqual(str(eveniment), "H123 20/12/2020 10:30 Nunta")
        eveniment1 = Eveniment(eveniment_id, None, None, None)
        self.assertTrue(eveniment == eveniment1)
        inscriere_id = "I1111"
        inscriere = Inscriere(inscriere_id, persoana, eveniment)
        self.assertEqual(inscriere.get_eveniment(), eveniment)
        self.assertEqual(inscriere.get_persoana(), persoana)
        self.assertEqual(inscriere.get_inscriere_id(), inscriere_id)

    def test_validari(self):
        persoana = Persoana("", "", "")
        validator_persoana = ValidatorPersoana()
        try:
            validator_persoana.valideaza(persoana)
            assert False
        except ValidatorExceptie as eroare_validare:
            self.assertEqual(str(eroare_validare), "ID invalid!\nNume invalid!\nAdresa invalida!\n")
        persoana_valida = Persoana("0003", "Nastea Andries", "str. Drochia nr.14")
        validator_persoana.valideaza(persoana_valida)
        assert True
        eveniment = Eveniment("", "", "", "")
        validator_eveniment = ValidatorEveniment()
        try:
            validator_eveniment.valideaza(eveniment)
            assert False
        except ValidatorExceptie as eroare_validare:
            self.assertEqual(str(eroare_validare), "ID invalid!\nData invalida!\nOra invalida!\nDescriere invalida!\n")
        eveniment_valid = Eveniment("N111", "2011.11.11", "11.33", "Nunta")
        validator_eveniment.valideaza(eveniment_valid)
        assert True
        inscriere_valida = Inscriere("I1021", persoana_valida, eveniment_valid)
        validator_inscriere = ValidatorInscriere()
        validator_inscriere.valideaza(inscriere_valida)

    def test_repository_persoana(self):
        """
        Teste RepositoryPersoana
        """
        persoana = Persoana("0003", "Nastea", "Drochia")
        repository_persoane = RepositoryPersoana()
        self.assertTrue(len(repository_persoane) == 0)
        repository_persoane.store(persoana)
        self.assertTrue(len(repository_persoane) == 1)
        persoana1 = Persoana("0017", "Hitler", "Munich")
        try:
            repository_persoane.store(persoana1)
            assert True
        except RepositoryExceptie:
            assert False
        repository_persoane.remove("0017")
        self.assertTrue(len(repository_persoane) == 1)
        try:
            repository_persoane.remove("0010")
            assert False
        except RepositoryExceptie as eroare_repository:
            self.assertTrue(str(eroare_repository) == "Persoana cautata nu se afla in lista de persoane!\n")
        lista_rezultat = repository_persoane.search_by_adress("Drochia")
        self.assertTrue(len(lista_rezultat) == 1)
        self.assertTrue(lista_rezultat[0].get_person_id() == "0003")
        self.assertTrue(lista_rezultat[0].get_nume() == "Nastea")
        self.assertTrue(lista_rezultat[0].get_adresa() == "Drochia")
        self.assertTrue(lista_rezultat[0].get_nume()[0] == "N")
        persoana_rezultat = repository_persoane.get_person_by_id("0003")
        self.assertTrue(persoana_rezultat.get_person_id() == "0003")

    def test_repository_eveniment(self):
        """
        Teste RepositoryEveniment
        """
        eveniment = Eveniment("N111", "11/11/2011", "11:33", "Nunta")
        repository_evenimente = RepositoryEveniment()
        self.assertTrue(len(repository_evenimente) == 0)
        repository_evenimente.store(eveniment)
        self.assertTrue(len(repository_evenimente) == 1)
        eveniment1 = Eveniment("K122", "07/05/2020", "13:25", "Zi de nastere")
        try:
            repository_evenimente.store(eveniment1)
            assert True
        except RepositoryExceptie:
            assert False
        eveniment1 = Eveniment("K123", "07/05/2020", "13:25", "Zi de nastere")
        try:
            repository_evenimente.update("K123", eveniment1)
            assert False
        except RepositoryExceptie as eroare_repository:
            self.assertTrue(str(eroare_repository) == "Evenimentul cautat nu se afla in lista de evenimente!\n")
        repository_evenimente.remove("K122")
        self.assertTrue(len(repository_evenimente) == 1)
        try:
            repository_evenimente.remove("K122")
            assert False
        except RepositoryExceptie as eroare_repository:
            self.assertTrue(str(eroare_repository) == "Evenimentul cautat nu se afla in lista de evenimente!\n")
        lista_rezultat = repository_evenimente.search_by_data("11/11/2011")
        self.assertTrue(len(lista_rezultat) == 1)
        self.assertTrue(lista_rezultat[0].get_data() == "11/11/2011")
        lista_rezultat = repository_evenimente.search_by_description("Nunta")
        self.assertTrue(len(lista_rezultat) == 1)
        self.assertTrue(lista_rezultat[0].get_descriere() == "Nunta")
        rezultat = repository_evenimente.get_eveniment_by_id("N111")
        self.assertTrue(rezultat.get_eveniment_id() == "N111")
        self.assertTrue(rezultat.get_data() == "11/11/2011")
        self.assertTrue(rezultat.get_timp() == "11:33")
        self.assertTrue(rezultat.get_descriere() == "Nunta")

    def test_service_persoana(self):
        """
        Teste ServicePersoana
        """
        repository_persoana = RepositoryPersoana()
        validator_persoana = ValidatorPersoana()
        service_persoana = ServicePersoana(repository_persoana, validator_persoana)
        person_id = "0012"
        nume = "Nicu Andries"
        adresa = "str. Suri nr.12"
        service_persoana.adaugare_persoana(person_id, nume, adresa)
        self.assertTrue(service_persoana.get_nr_persoane() == 1)
        try:
            service_persoana.adaugare_persoana(person_id, nume, adresa)
            assert False
        except RepositoryExceptie as eroare_repository:
            self.assertTrue(str(eroare_repository) == "Exista deja o persoana cu aceleasi date!\n")
        try:
            service_persoana.adaugare_persoana("", "", adresa)
            assert False
        except ValidatorExceptie as eroare_validare:
            self.assertTrue(str(eroare_validare) == "ID invalid!\nNume invalid!\n")
        rezultat = service_persoana.cautare_dupa_litera("N")
        self.assertTrue(len(rezultat) == 1)
        for i in range(0, len(rezultat)):
            self.assertTrue(rezultat[i].get_nume()[0] == "N")
        rezultat = service_persoana.cautare_dupa_adresa("str. Suri nr.12")
        self.assertTrue(len(rezultat) == 1)
        for i in range(0, len(rezultat)):
            self.assertTrue(rezultat[i].get_adresa() == "str. Suri nr.12")

    def test_service_eveniment(self):
        """
        Teste ServiceEveniment
        """
        repository_evenimente = RepositoryEveniment()
        validator_eveniment = ValidatorEveniment()
        service_eveniment = ServiceEveniment(repository_evenimente, validator_eveniment)
        eveniment_id = "H123"
        data = "2020.12.20"
        timp = "10.30"
        descriere = "Nunta"
        service_eveniment.adaugare_eveniment(eveniment_id, data, timp, descriere)
        self.assertTrue(service_eveniment.get_nr_evenimente() == 1)
        try:
            service_eveniment.adaugare_eveniment(eveniment_id, data, timp, descriere)
            assert False
        except RepositoryExceptie as eroare_repository:
            self.assertTrue(str(eroare_repository) == "Exista deja un eveniment cu aceleasi date!\n")
        try:
            service_eveniment.adaugare_eveniment("", "2020.12.20", "10.30", "")
            assert False
        except ValidatorExceptie as eroare_validare:
            self.assertTrue(str(eroare_validare) == "ID invalid!\nDescriere invalida!\n")
        rezultat = service_eveniment.cautare_dupa_data("2020.12.20")
        self.assertTrue(len(rezultat) == 1)
        for i in range(0, len(rezultat)):
            self.assertTrue(rezultat[i].get_data() == "2020.12.20")
        rezultat = service_eveniment.cautare_dupa_descriere("Nunta")
        self.assertTrue(len(rezultat) == 1)
        for i in range(0, len(rezultat)):
            self.assertTrue(rezultat[i].get_descriere() == "Nunta")

    def test_repository_inscriere(self):
        repository_inscriere = RepositoryInscriere()
        repository_evenimente = RepositoryEveniment()
        persoana = Persoana("0003", "Nastea Laptedulce", "str. Bucuresti nr.15")
        eveniment = Eveniment("K122", "07/05/2020", "13:25", "Zi de nastere")
        repository_evenimente.store(eveniment)
        inscriere = Inscriere("I1502", persoana, eveniment)
        self.assertTrue(len(repository_inscriere.get_all()) == 0)
        repository_inscriere.adaugare(inscriere)
        self.assertTrue(len(repository_inscriere.get_all()) == 1)
        eveniment_id = repository_inscriere.lista_evenimente_persoana("0003")
        rezultat = [repository_evenimente.get_eveniment_by_id(eveniment_id[0])]
        self.assertTrue(len(rezultat) == 1)
        self.assertTrue(rezultat[0].get_eveniment_id() == "K122")
        self.assertTrue(rezultat[0].get_data() == "07/05/2020")
        self.assertTrue(rezultat[0].get_timp() == "13:25")
        self.assertTrue(rezultat[0].get_descriere() == "Zi de nastere")
        repository_inscriere.update_inscriere_eveniment_by_id("I1502", "K122")
        lista_evenimente_persoana = repository_inscriere.lista_evenimente_persoana("0003")
        self.assertTrue(len(lista_evenimente_persoana) == 1)
        self.assertEqual(lista_evenimente_persoana[0], "K122")
        repository_inscriere.remove_inscriere_by_id("I1502")
        self.assertTrue(len(repository_inscriere.get_all()) == 0)

    def test_service_inscriere(self):
        repository_persoana = RepositoryPersoana()
        repository_eveniment = RepositoryEveniment()
        repository_inscriere = RepositoryInscriere()
        validator_inscriere = ValidatorInscriere()
        service_inscriere = ServiceInscriere(repository_persoana, repository_eveniment, repository_inscriere, validator_inscriere)
        persoana = Persoana("0003", "Nastea Laptedulce", "str. Bucuresti nr.15")
        repository_persoana.store(persoana)
        eveniment = Eveniment("K122", "07/05/2020", "13:25", "Zi de nastere")
        repository_eveniment.store(eveniment)
        service_inscriere.adaugare_inscriere("0003", "K122")
        persoana = Persoana("0015", "Anastasia Laptedulce", "str. Bucuresti nr.15")
        repository_persoana.store(persoana)
        eveniment = Eveniment("K123", "07/05/2020", "13:25", "Zi de nastere")
        repository_eveniment.store(eveniment)
        service_inscriere.adaugare_inscriere("0015", "K123")
        service_inscriere.get_inscrieri_nou()
        self.assertTrue(len(service_inscriere.get_inscrieri_nou()) == 2)
        person_id_vechi = "0003"
        persoana_noua = Persoana("0001", "Nastea Laptedulce", "str. Bucuresti nr.15")
        service_inscriere.modificare_persoana(person_id_vechi, persoana_noua)
        self.assertEqual(service_inscriere.get_inscrieri_nou()[1].get_persoana().get_person_id(), persoana_noua.get_person_id())
        eveniment_id_vechi = "K122"
        eveniment_nou = Eveniment("L111", "2022.05.18", "13.25", "Concert")
        service_inscriere.modificare_eveniment(eveniment_id_vechi, eveniment_nou)
        self.assertEqual(service_inscriere.get_inscrieri_nou()[1].get_eveniment().get_eveniment_id(), eveniment_nou.get_eveniment_id())
        self.assertTrue(service_inscriere.numarul_de_evenimente_persoana()[0][1] == 1)
        raport1 = service_inscriere.afisare_evenimente_persoana_sortat_descriere("0001")
        self.assertTrue(len(raport1) == 1)
        self.assertEqual(raport1[0].get_descriere(), "Concert")
        raport2 = service_inscriere.afisare_evenimente_persoana_sortat_descriere("0001")
        self.assertTrue(len(raport2) == 1)
        self.assertEqual(raport2[0].get_data(), "2022.05.18")
        service_inscriere.remove_inscriere_si_persoana_by_id("0001")
        self.assertTrue(len(service_inscriere.get_inscrieri_nou()) == 1)
        service_inscriere.remove_inscriere_si_evenimente_by_id("K123")
        self.assertTrue(len(service_inscriere.get_inscrieri_nou()) == 0)

    def test_black_box_generare_persoane(self):
        repository_persoana = RepositoryPersoana()
        validator_persoana = ValidatorPersoana()
        service_persoana = ServicePersoana(repository_persoana, validator_persoana)
        with self.assertRaises(ServiceExceptie):
            service_persoana.adaugare_persoana_random(-1)
        with self.assertRaises(TypeError):
            service_persoana.adaugare_persoana_random("a")
        self.assertEqual(service_persoana.get_nr_persoane(), 0)
        service_persoana.adaugare_persoana_random(3)
        self.assertEqual(service_persoana.get_nr_persoane(), 3)

    def test_white_box_generare_persoane(self):
        repository_persoana = RepositoryPersoana()
        validator_persoana = ValidatorPersoana()
        service_persoana = ServicePersoana(repository_persoana, validator_persoana)
        with self.assertRaises(ServiceExceptie):
            service_persoana.adaugare_persoana_random(-1)
        self.assertEqual(service_persoana.get_nr_persoane(), 0)
        with self.assertRaises(TypeError):
            service_persoana.adaugare_persoana_random("a")
        with self.assertRaises(ServiceExceptie):
            service_persoana.adaugare_persoana_random(0)
        self.assertEqual(service_persoana.get_nr_persoane(), 0)
        service_persoana.adaugare_persoana_random(3)
        self.assertEqual(service_persoana.get_nr_persoane(), 3)
        service_persoana.adaugare_persoana_random(6)
        self.assertEqual(service_persoana.get_nr_persoane(), 9)
        service_persoana.adaugare_persoana_random(2)
        self.assertEqual(service_persoana.get_nr_persoane(), 11)

    def test_store_to_file(self):
        repository_persoana = PersoanaFileRepository("persoane_testare.txt")
        with open("persoane_testare.txt", "w+") as fisier:
            persoana = Persoana("0023", "Nicu Andries", "str. Bucuresti nr.22")
            repository_persoana.store(persoana)
            fisier.write(str(persoana.get_person_id()) + ";" + str(persoana.get_nume()) + ";" + str(persoana.get_adresa()) + ";\n")
            self.assertEqual(len(repository_persoana.get_all()), 1)
            repository_persoana.remove_all()
            self.assertEqual(len(repository_persoana.get_all()), 0)

    def test_load_from_file(self):
        repository_persoana = PersoanaFileRepository("persoane_testare.txt")
        with open("persoane_testare.txt", "r") as fisier:
            repository_persoana.remove_all()
            persoana = Persoana("0003", "Nastea Laptedulce", "str. Bucuresti nr.15")
            repository_persoana.store(persoana)
            lines = fisier.readlines()
            self.assertEqual(lines[0].strip(), "0003;Nastea Laptedulce;str. Bucuresti nr.15;")


if __name__ == '__main__':
    unittest.main()
