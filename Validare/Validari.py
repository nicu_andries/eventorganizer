from Erori.Exceptii import ValidatorExceptie
import re


class ValidatorPersoana:
    """
    Clasa de validare a unei persoane
    """

    @staticmethod
    def valideaza(persoana):
        """
        Metoda de validare a unei persoane
        """
        erori = ""
        if not re.fullmatch(r"[0-9][0-9][0-9][0-9]", str(persoana.get_person_id())):
            erori = erori + "ID invalid!\n"
        if not re.fullmatch(r"[A-Z][a-z]{1,25}\s[A-Z][a-z]{1,25}?", str(persoana.get_nume())):
            erori = erori + "Nume invalid!\n"
        if not re.fullmatch(r"[s][t][r]\.\s[A-Z][a-z]{2,25}\s[n][r]\.[0-9][0-9]?", str(persoana.get_adresa())):
            erori = erori + "Adresa invalida!\n"
        if len(erori) > 0:
            raise ValidatorExceptie(erori)

    @staticmethod
    def valideaza_id(person_id):
        """
        Metoda de validare a ID-ului
        """
        erori = ""
        if not re.fullmatch(r"[0-9][0-9][0-9][0-9]", person_id):
            erori = erori + "ID invalid!\n"
        if len(erori) > 0:
            raise ValidatorExceptie(erori)


class ValidatorEveniment:

    @staticmethod
    def valideaza(eveniment):
        """
        Metoda de validare a unui eveniment
        """

        erori = ""
        if not re.fullmatch(r"[A-Z][0-9][0-9][0-9]", str(eveniment.get_eveniment_id())):
            erori = erori + "ID invalid!\n"
        if not re.fullmatch(r"[2-9][0-9][0-9][0-9]\.[0-1][0-9]\.[0-3][0-9]", str(eveniment.get_data())):
            erori = erori + "Data invalida!\n"
        if not re.fullmatch(r"[0-2][0-9]\.[0-5][0-9]", str(eveniment.get_timp())):
            erori = erori + "Ora invalida!\n"
        if not str(eveniment.get_descriere()).isalpha():
            erori = erori + "Descriere invalida!\n"
        if len(erori) > 0:
            raise ValidatorExceptie(erori)

    @staticmethod
    def valideaza_id(eveniment_id):
        """
        Metoda de validare a ID-ului
        """
        erori = ""
        if not re.fullmatch(r"[A-Z][0-9][0-9][0-9]", eveniment_id):
            erori = erori + "ID invalid!\n"
        if len(erori) > 0:
            raise ValidatorExceptie(erori)


class ValidatorInscriere:
    """
    Clasa de validare a unei inscrieri
    """

    @staticmethod
    def valideaza(inscriere):
        """
        Metoda de validare a unei inscrieri
        """
        erori = ""
        if str(inscriere.get_persoana().get_person_id()) == "" or not re.fullmatch(r"[0-9][0-9][0-9][0-9]", str(inscriere.get_persoana().get_person_id())):
            erori = erori + "ID persoana invalid!\n"
        if str(inscriere.get_eveniment().get_eveniment_id()) == "" or not re.fullmatch(r"[A-Z][0-9][0-9][0-9]", str(inscriere.get_eveniment().get_eveniment_id())):
            erori = erori + "ID eveniment invalid!\n"
        if len(erori) > 0:
            raise ValidatorExceptie(erori)
